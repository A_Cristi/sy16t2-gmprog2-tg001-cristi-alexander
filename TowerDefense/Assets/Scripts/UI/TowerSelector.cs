﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerSelector : MonoBehaviour {

	public Text Select;

	private string Tower;
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			Tower = "Arrow";

		} else if (Input.GetKeyDown (KeyCode.Alpha2)) {
			Tower = "Cannon";
				
		} else if (Input.GetKeyDown (KeyCode.Alpha3)) {
			Tower = "Ice";

		} else if (Input.GetKeyDown (KeyCode.Alpha4)) {
			Tower = "Fire";

		}

		Select.text = "Selected : " + Tower;
	}
}
