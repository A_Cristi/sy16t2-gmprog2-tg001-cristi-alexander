﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gold : MonoBehaviour {

	public int BaseGold = 1000;
	public Text GoldText;

	void Update()
	{
		GoldText.text = "Gold -- " + BaseGold.ToString ();
	}

	public void SubtractGold(int amt)
	{
		BaseGold -= amt;
	}

	public bool CheckGold(int amt)
	{
		if (BaseGold >= amt)
			return true;
		else
			return false;
	}

}