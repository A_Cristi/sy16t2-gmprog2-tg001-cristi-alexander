﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Apocalypse : MonoBehaviour {

	public Text ApoText;
	public float CD = 60;

	private float CDMan = 0;
	private bool isCD = false;
	private GameObject[] Enemies;

	void Update()
	{				
		if (isCD == false) {

			if (Input.GetKeyDown (KeyCode.Q)) {
				Activate ();


				isCD = true;	
				CDMan = CD;
			}
		}

		else{
			CDMan -= Time.deltaTime;
			if (CDMan <= 0) {
				CDMan = 0.0f;
				isCD = false;
			}

		}
		ApoText.text = "Q-Apocalypse " + CDMan.ToString ();
 	}

	void Activate()
	{
		Debug.Log ("ACTIVATED");

		Enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		for (int i = 0; i < Enemies.Length; i++) {
			//Enemies [i].GetComponent<EnemyHealth>().HP = 0;
			Enemies [i].GetComponent<EnemyHealth>().TakeDam(1000);
		}
	}
}
