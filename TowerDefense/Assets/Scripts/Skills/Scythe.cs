﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Scythe : MonoBehaviour {
	
	public Text ScytheT;
	public float CD = 10;
	public GameObject Scyth;

	private float CDMan = 0;
	private bool isCD = false;
	private GameObject[] Enemies;

	void Update()
	{				
		if (isCD == false) {

			if (Input.GetKeyDown (KeyCode.W)) {
				Activate ();


				isCD = true;	
				CDMan = CD;
			}
		}

		else{
			CDMan -= Time.deltaTime;
			if (CDMan <= 0) {
				CDMan = 0.0f;
				isCD = false;
			}

		}
		ScytheT.text = "W-Scythe " + CDMan.ToString ();
	}

	void Activate()
	{
		Debug.Log ("ACTIVATED");

	}
}
