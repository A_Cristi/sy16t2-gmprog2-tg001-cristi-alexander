﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Meteor : MonoBehaviour {

	public Text MeteorT;
	public float CD = 20;
	public GameObject MeteorPrefab;

	private float CDMan = 0;
	private bool isCD = false;
    bool IsClicked = false;

    void Update()
	{				
		if (isCD == false) {

			if (Input.GetKeyDown (KeyCode.E)) {
				Activate ();


				isCD = true;	
				CDMan = CD;
			}
		}

		else{
			CDMan -= Time.deltaTime;
			if (CDMan <= 0) {
				CDMan = 0.0f;
				isCD = false;
                IsClicked = false;
			}

		}
		MeteorT.text = "E-Meteor " + CDMan.ToString ();
	}

	void Activate()
	{
		Debug.Log ("ACTIVATED");	

		while (IsClicked = false) {
			if (Input.GetKeyDown(KeyCode.Space)) {

                Vector3 V = Input.mousePosition;
                V.y = 20;
                V = Camera.main.ScreenToWorldPoint(V);
                Instantiate(MeteorT, V, Quaternion.identity);

                IsClicked = true;
            }



		}


	}

}
