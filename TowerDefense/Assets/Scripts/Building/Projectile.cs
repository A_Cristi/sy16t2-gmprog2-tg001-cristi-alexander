﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	TowerData DTA;

	private int TowerLevel;

	private int DMG;	

	void Start()
	{
		DMG = 10;
	}

	void Update()
	{
		CheckLevel ();

	}

	void OnCollisionEnter(Collider col)
	{
		col.GetComponent<EnemyHealth> ().TakeDam (DMG);
	}

	void CheckLevel()
	{
		switch (DTA.TWRLevel) {
		case 1:
			DMG = 10;
			break;

		case 2: 
			DMG = 20;
			break;

		case 3:
			DMG = 330;
			break;

		}

	}
}
