﻿ using UnityEngine;
 using System.Collections;

public class FireBullet : MonoBehaviour
{    
    public Transform Enemy;

    public float range = 50.0f;
    public float bulletImpulse = 20.0f;
    public Rigidbody projectile;

    private bool onRange = false;
    private float Rate;
    public float FireRate;


    void Start()
    {     
		Rate = FireRate;        
    } 

    void Update()
    {
		if (Enemy != null && Rate > 0)
        {
			Rate -= Time.deltaTime;
        }
		if (Rate <= 0 && Enemy != null)
        {
            onRange = Vector3.Distance(transform.position, Enemy.transform.position) < range;
            if (onRange)
            {
				Fire();
				Rate = FireRate;
            }
        }
    }
	void Fire()
	{
		Rigidbody bullet = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
		bullet.velocity = (Enemy.position - transform.position).normalized * bulletImpulse;

		Destroy(bullet.gameObject, 0.5f);

	}
}

