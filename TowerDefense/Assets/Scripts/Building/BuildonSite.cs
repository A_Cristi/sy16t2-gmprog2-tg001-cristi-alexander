﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildonSite : MonoBehaviour {
	public GameObject ArrowTower;
	public GameObject CanonTower;
	public GameObject IceTower;
	public GameObject FireTower;

	private GameObject Selected;

	private bool HasBuilding;
	private int TowerCost;

	Gold gold;

	public GameObject GoldHolder;

	void Start()
	{
		HasBuilding = false;

		gold = GoldHolder.GetComponent<Gold> ();
	}

	void Update()
	{
		
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			Selected = ArrowTower;
			TowerCost = 200;

		} else if (Input.GetKeyDown (KeyCode.Alpha2)) {
			Selected = CanonTower;
			TowerCost = 300;

		} else if (Input.GetKeyDown (KeyCode.Alpha3)) {
			Selected = IceTower;
			TowerCost = 600;

		} else if (Input.GetKeyDown (KeyCode.Alpha4)) {
			Selected = FireTower;
			TowerCost = 600;
		}
	}

	void OnMouseUpAsButton()
	{
		Vector3 pos = new Vector3(0.0f, 0.78f, 0.0f);

		if (gold.CheckGold (TowerCost) == true) {

			if (HasBuilding == false) {
				GameObject TWR = (GameObject)Instantiate (Selected);
				TWR.transform.position = transform.position;
				TWR.transform.position += pos;
				HasBuilding = true;
				gold.SubtractGold (TowerCost);

			} else {

			}
		}

	}



}
