﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour
{
	public GameObject Target;
	public GameObject[] Enemies;
	public EType TargetType;

	public float TwrRange = 10.0f;
	private bool IsInRange = false;

	void Update ()
	{
		Detect ();
	}

	void Detect ()
	{
		Enemies = GameObject.FindGameObjectsWithTag ("Enemy");


		int closest = 0;
		float nearestDistance = Mathf.Infinity;
		float closestEnemy;
		for (int i = 0; i < Enemies.Length; i++)
        {
			IsInRange = Vector3.Distance (transform.position, Enemies [i].transform.position) < TwrRange;
			closestEnemy = Vector3.Distance (transform.position, base.transform.position);

			if (closestEnemy < nearestDistance && IsInRange)
            {				
				nearestDistance = closestEnemy;
				closest = i;
				Target = Enemies [i];             
               
				if(Target.GetComponent<EnemyType>().Type != TargetType){
					Target = null;
                    return;
                }

				transform.LookAt(Target.transform);
            }            
		}
		if (Target != null) {
			Debug.Log ("TARGET");
			GetComponent<FireBullet> ().Enemy = Target.transform;
		} else {
			Debug.Log ("NO TARGET");
			GetComponent<FireBullet> ().Enemy = null;
		}
	}
}
    