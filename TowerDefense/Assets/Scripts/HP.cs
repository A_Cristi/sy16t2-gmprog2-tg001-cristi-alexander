﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour {

	public int Health;

	public Text HPText;

	void Update()
	{
		HPText.text = "Life -- " + Health.ToString();

		if (Health <= 0) {
			Application.Quit();
		}


	}


}
