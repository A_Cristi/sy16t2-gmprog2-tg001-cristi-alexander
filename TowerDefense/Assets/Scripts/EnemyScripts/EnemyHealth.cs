﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {
	
	public int HP = 100;
	Gold gold;

	void update()
	{
		CheckHP ();
	}

	public void TakeDam(int dmg)
	{
		HP -= dmg;
	}

	void CheckHP()
	{
		if (HP <= 0) {
			Debug.Log ("DEAD");
			gold.BaseGold += 100;
			Destroy (gameObject);
		}


		if (Input.GetKeyDown (KeyCode.A)) {
			Destroy (this.gameObject);
		}
	}

	bool isLower15(int HP)
	{
		float p = 15 / HP;

		return true;

	}
}
