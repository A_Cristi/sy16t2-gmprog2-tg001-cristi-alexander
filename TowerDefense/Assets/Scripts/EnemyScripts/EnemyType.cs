﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EType
{
	Ground, Flying, Boss
}


public class EnemyType : MonoBehaviour {    
    public EType Type;
}
