﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {


	void Start () 
	{
		GameObject end = GameObject.Find("End");

		UnityEngine.AI.NavMeshAgent nav = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		nav.destination = end.transform.position;

	}


}
