﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    public float speed;   

    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -29.5f, 29.5f), 0.5f,
                                         Mathf.Clamp(transform.position.z, -19.5f, 19.5f));

        Vector3 MousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePoint.y = transform.position.y;

        transform.position = Vector3.MoveTowards(transform.position, MousePoint, speed * Time.deltaTime);     
        

    }

}
