﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerShip : MonoBehaviour {
    public float MoveSpeed = 1.0f;
    public int Life = 3;

	public GameObject bullet;
	public Transform spawner;

	public float fireRate;
	private float nextFire;

    public float xMin, xMax, yMin, yMax;
    //[Header("UI")]
    //public Text LifeText;
    //public Text KillCountText;   

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * MoveSpeed * Time.deltaTime;

        //LifeText.text = "Life: " + Life;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -10.3f, 10.3f),
                                         Mathf.Clamp(transform.position.y, -8.0f, 10.5f),
                                         -1.0f);

        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			Instantiate(bullet, spawner.position, spawner.rotation);
		}

        

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Asteroid") return;

        // Deduct life
        Life--;
        //if (Life <= 0) Destroy(gameObject);

        // Destroy the asteroid
        Destroy(other.gameObject);
    }
}
