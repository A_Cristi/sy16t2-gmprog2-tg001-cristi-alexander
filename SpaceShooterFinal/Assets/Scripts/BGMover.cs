﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMover : MonoBehaviour {

    public Transform target;
    public float speed;

    void Update ()
    {
        float spd = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, spd);

        if (transform.position.y == target.position.y)
        {
            transform.position = new Vector3(0, 0, 0);
        }


    }
}
