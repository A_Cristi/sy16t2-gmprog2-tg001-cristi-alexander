﻿using UnityEngine;
using System.Collections;

public class PlayerClamp : MonoBehaviour {

    void Update()
    {
        Vector3 PlayerLock = transform.position;
        PlayerLock.x = Mathf.Clamp(transform.position.x, -30.0f, 30.0f);
        PlayerLock.z = Mathf.Clamp(transform.position.z, -30.0f, 30.0f);
        PlayerLock.y = 0.5f;
        transform.position = PlayerLock;

    }

}
