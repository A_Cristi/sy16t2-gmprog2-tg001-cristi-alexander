﻿ using UnityEngine;
using System.Collections;

public class AIState : MonoBehaviour {

    enum State {FindFood, RunFromPlayer, RunFromAI, ChasePlayer, ChaseAI }

    public Vector3 Size = new Vector3(0.2f, 0.2f, 0.2f);

    State CurState;

    float TimeSinceLastChange = 0.0f;

    float DistanceToFood,DistanceToPlayer,DistanceToOtherAI;

    public float Speed = 3.0f;

    void Start ()
    {
        CalculateDistances();
        SetState(State.FindFood);

        transform.localScale = Size;
    } 
	
	void Update ()
    {
        
        CalculateDistances();
        AssignState();
        Action(CurState);   
    }

    void CalculateDistances()
    {
        DistanceToFood = Vector3.Distance(transform.position, FindClosestObj("Food").transform.position);
        DistanceToPlayer = Vector3.Distance(transform.position, FindClosestObj("Player").transform.position);
        DistanceToOtherAI = Vector3.Distance(transform.position, FindClosestObj("Enemy").transform.position);

    }

    GameObject FindClosestObj(string Tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(Tag);
        GameObject closest = null;

        float distance = Mathf.Infinity;
        //float distance = 10.0f;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    void SetState(State st)
    {
        CurState = st;
        TimeSinceLastChange = Time.time;
    }

    //float GetStateElapsed()
    //{
    //    return Time.time - TimeSinceLastChange;
    //}

    void AssignState()
    {
        if ((DistanceToOtherAI < DistanceToPlayer || DistanceToOtherAI < DistanceToFood)
             && transform.localScale.x > FindClosestObj("Enemy").transform.localScale.x)
        
            SetState(State.ChaseAI);
        

        else if ((DistanceToPlayer < DistanceToOtherAI || DistanceToPlayer < DistanceToFood)
            && transform.localScale.x > FindClosestObj("Player").transform.localScale.x)
        
            SetState(State.ChasePlayer);
        

        else if ((DistanceToPlayer < DistanceToOtherAI || DistanceToPlayer < DistanceToFood)
            && transform.localScale.x < FindClosestObj("Player").transform.localScale.x)

            SetState(State.RunFromPlayer);

        else if ((DistanceToPlayer < DistanceToOtherAI || DistanceToPlayer < DistanceToFood)
            && transform.localScale.x < FindClosestObj("Enemy").transform.localScale.x)

            SetState(State.RunFromAI);

        else
            SetState(State.FindFood);
    }

    void Action(State st)
    {
        switch(st)
        {
            case State.ChaseAI:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Enemy").transform.position, Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.ChasePlayer:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Player").transform.position, Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.FindFood:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Food").transform.position, Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.RunFromPlayer:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Player").transform.position, -Speed * Time.deltaTime / transform.localScale.x);
                break;

            case State.RunFromAI:
                transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Enemy").transform.position, -Speed * Time.deltaTime / transform.localScale.x);
                break;

        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food")
        {
            Destroy(other.gameObject);

            transform.localScale += Size;
        }

        if (other.gameObject.tag == "Enemy")
        {
            if(other.transform.localScale.x < transform.localScale.x)
            {
                Destroy(other.gameObject);

                transform.localScale += Size;
            }           

            else
            {
                Destroy(this);
            }

        }

    }
}
