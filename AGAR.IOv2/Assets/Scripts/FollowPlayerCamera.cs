﻿using UnityEngine;
using System.Collections;

public class FollowPlayerCamera : MonoBehaviour {

    public GameObject target;
    public float speed = 5.0f;

    private float Distance;

    private float Scale;

    void Start()
    {
        Scale = target.transform.localScale.x;

    }
     
	void Update ()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);

        if(target.transform.localScale.x > Scale)
        {
            Camera.main.orthographicSize += target.transform.localScale.x;            
            Scale = target.transform.localScale.x;
        }

        if (target.transform.localScale.x < Scale)
        {
            Camera.main.orthographicSize -= target.transform.localScale.x;
            Scale = target.transform.localScale.x;

        }

        //transform.position = Vector3.Lerp(transform.position, target.position, 1.0f);

    }
}
