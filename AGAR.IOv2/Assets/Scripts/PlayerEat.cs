﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerEat : MonoBehaviour {

    public Vector3 FoodSize = new Vector3(0.2f, 0.2f, 0.2f);

    private int Score = 0;

    public Text ScoreText;

    void Start()
    {
        ScoreText.text = "Score : " + Score.ToString();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus)) transform.localScale += FoodSize;
        if (Input.GetKeyDown(KeyCode.KeypadMinus)) transform.localScale -= FoodSize;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food")
        {
            Destroy(other.gameObject);

            transform.localScale += FoodSize;
            Score++;
            ScoreText.text = "Score : " + Score.ToString();

        }

        if (other.gameObject.tag == "Enemy")
        {

            if (FindClosestObj("Enemy").transform.localScale.x > transform.localScale.x)
            {
                Destroy(this.gameObject);
            }

            else
            {
                transform.localScale += FindClosestObj("Enemy").transform.localScale;

                Destroy(FindClosestObj("Enemy"));
            }


        }


        Debug.Log(Score);

        Debug.Log(transform.localScale);

    }

    GameObject FindClosestObj(string Tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(Tag);
        GameObject closest = null;

        float distance = Mathf.Infinity;
        //float distance = 10.0f;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

}
