﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Debug.Log("Movement Started");
        
        Debug.Log(transform.position);
	
	}
	
	// Update is called once per frame
	void Update () {
        float direction = Input.GetAxis("Horizontal");
        transform.position += new Vector3(direction, 0, 0) * Time.deltaTime;

        if(Input.GetKeyDown(KeyCode.Space))
        {
            Rigidbody body = GetComponent<Rigidbody>();

            body.AddForce(new Vector3(0, 500, 0));
        }

    }
}
