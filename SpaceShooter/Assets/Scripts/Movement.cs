﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    //// Use this for initialization
    //void Start () {

    //       Debug.Log("Movement Started");

    //       Debug.Log(transform.position);

    //   }

    //// Update is called once per frame
    //void Update () {

    //       float MoveHorizontal = Input.GetAxis("Horizontal");
    //       float MoveVertical = Input.GetAxis("Vertical");

    //       transform.position += new Vector3(MoveHorizontal, 0, MoveVertical) * Time.deltaTime;

    //   }

    public float Movespeed = 1.0f;

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * Movespeed * Time.deltaTime;

    }

}
